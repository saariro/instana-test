package com.instana;

public class TraceNotFoundException extends RuntimeException {
    public TraceNotFoundException(String message) {
        super(message);
    }

}
