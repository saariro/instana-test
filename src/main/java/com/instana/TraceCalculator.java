package com.instana;

import java.io.*;
import java.util.*;

public class TraceCalculator {

    public TraceCalculator(){ }

    public HashMap<Character, HashMap<Character, Integer>> graph;

    /**
    *
    * TASKS 1-5 BELOW
    *
    * */

    public int calculateAverageTrace(List<Character> nodes) {
        ArrayList<Character> nodesLeft = new ArrayList<>(nodes);
        nodesLeft.remove(0);
        int traceLength = recursiveDFSAverageTraceCalculator(nodesLeft, nodes.get(0), 0);
        if (traceLength == 0)  {
            throw new TraceNotFoundException("NO SUCH TRACE");
        }
        return traceLength;
    }

    private int recursiveDFSAverageTraceCalculator(ArrayList<Character> nodes, Character currentNode, Integer currentDistance) {
        ArrayList<Character> nodesLeft = new ArrayList<>(nodes);
        if (nodesLeft.isEmpty()) {
            return currentDistance;
        }
        if (!graph.containsKey(currentNode)) {
            return 0;
        }
        for (Character targetNode : graph.get(currentNode).keySet()) {
            if (targetNode.compareTo(nodes.get(0)) == 0) {
                nodesLeft.remove(0);
                return recursiveDFSAverageTraceCalculator(nodesLeft, targetNode, currentDistance + graph.get(currentNode).get(targetNode));
            }
        }
        return 0;
    }


    /**
     *
     * TASK 6 BELOW
     *
     * */

    public int calculateTracesWithMaxHops(Character startNode, Character endNode, Integer maxHops) {
        int foundRoutes = recursiveDFSAtMostXHopsCalculator(endNode, startNode, maxHops, new ArrayList(), 0, 0);
        if (foundRoutes == 0)  {
            throw new TraceNotFoundException("NO SUCH TRACE");
        }
        return foundRoutes;
    }

    private int recursiveDFSAtMostXHopsCalculator(Character endNode, Character currentNode, Integer maxHops, List<Character> visitedNodes, Integer currentDistance, Integer currentHops) {
        ArrayList<Character> visitedNodesCopy = new ArrayList<>(visitedNodes);
        visitedNodesCopy.add(currentNode);
        int amountOfRoutes = 0;
        if (currentNode.compareTo(endNode) == 0 && currentDistance > 0) {
            return 1;
        }
        if (!graph.containsKey(currentNode) || Objects.equals(currentHops, maxHops)) {
            return 0;
        }
        for (Character targetNode : graph.get(currentNode).keySet()) {
            if (!visitedNodes.contains(targetNode) || targetNode.compareTo(endNode) == 0) {
                amountOfRoutes += recursiveDFSAtMostXHopsCalculator( endNode, targetNode, maxHops, visitedNodesCopy, currentDistance + graph.get(currentNode).get(targetNode), currentHops +1);
            }
        }
        return amountOfRoutes;
    }

    /**
     *
     * TASK 7 BELOW
     *
     * */

    public int calculateTracesWithExactHops(Character startNode, Character endNode, Integer hopAmount) {
        int foundRoutes = recursiveDFSExactAmountOfHopsCalculator(endNode, startNode, hopAmount, 0, 0);
        if (foundRoutes == 0)  {
            throw new TraceNotFoundException("NO SUCH TRACE");
        }
        return foundRoutes;
    }

    private int recursiveDFSExactAmountOfHopsCalculator(Character endNode, Character currentNode, Integer amountOfHops, Integer currentDistance, Integer currentHops) {
        int amountOfRoutes = 0;
        if (currentNode.compareTo(endNode) == 0 && currentDistance > 0 && Objects.equals(currentHops, amountOfHops)) {
            return 1;
        }
        if (!graph.containsKey(currentNode) || Objects.equals(currentHops, amountOfHops)) {
            return 0;
        }
        for (Character targetNode : graph.get(currentNode).keySet()) {
            amountOfRoutes += recursiveDFSExactAmountOfHopsCalculator( endNode, targetNode, amountOfHops, currentDistance + graph.get(currentNode).get(targetNode), currentHops +1);
        }
        return amountOfRoutes;
    }

    /**
     *
     * TASK 8-9 BELOW
     *
     * */

    public int calculateShortedTrace(Character startNode, Character endNode) {
        int traceLength = recursiveDFSShortestTraceCalculator(endNode, startNode, new ArrayList(), 0);
        if (traceLength == Integer.MAX_VALUE)  {
            throw new TraceNotFoundException("NO SUCH TRACE");
        }
        return traceLength;
    }

    private int recursiveDFSShortestTraceCalculator(Character endNode, Character currentNode, List<Character> visitedNodes, Integer currentDistance) {
        ArrayList<Character> visitedNodesCopy = new ArrayList<>(visitedNodes);
        visitedNodesCopy.add(currentNode);
        int minDistance = Integer.MAX_VALUE;
        if (currentNode.compareTo(endNode) == 0 && currentDistance > 0) {
            return currentDistance;
        }
        if (!graph.containsKey(currentNode)) {
            return Integer.MAX_VALUE;
        }
        for (Character targetNode : graph.get(currentNode).keySet()) {
            if (!visitedNodes.contains(targetNode) || targetNode.compareTo(endNode) == 0) {
                int foundDistance = recursiveDFSShortestTraceCalculator( endNode, targetNode, visitedNodesCopy, currentDistance + graph.get(currentNode).get(targetNode));
                minDistance = Math.min(minDistance, foundDistance);
            }
        }
        return minDistance;
    }


    /**
     *
     * TASK 10 BELOW
     *
     * */

    public int calculateAmountWithOfTracesWithMaxLatency(Character startNode, Character endNode, Integer maxLatency) {
        int traceLength = recursiveDFSMaxLatencyCalculator(endNode, startNode, maxLatency, 0);
        if (traceLength == 0)  {
            throw new TraceNotFoundException("NO SUCH TRACE");
        }
        return traceLength;
    }

    private int recursiveDFSMaxLatencyCalculator(Character endNode, Character currentNode, Integer maxLatency, Integer currentDistance) {
        int amountOfRoutes = 0;
        if (currentNode.compareTo(endNode) == 0 && currentDistance > 0) {
            amountOfRoutes++;
        }
        if (!graph.containsKey(currentNode)) {
            return amountOfRoutes;
        }
        for (Character targetNode : graph.get(currentNode).keySet()) {
            if (currentDistance + graph.get(currentNode).get(targetNode) < maxLatency) {
                amountOfRoutes += recursiveDFSMaxLatencyCalculator( endNode, targetNode, maxLatency,currentDistance + graph.get(currentNode).get(targetNode));
            }
        }
        return amountOfRoutes;
    }


    /**
     *
     * Utility Functions BELOW
     *
     * */

    public void buildGraph(HashMap<String, Integer> edges) {
        graph = new HashMap<>();
        edges.forEach((key, value) -> {
            if (graph.get(key.charAt(0)) == null) {
                HashMap<Character, Integer> subMap = new HashMap<>();
                subMap.put(key.charAt(1), value);
                graph.put(key.charAt(0), subMap);
            } else {
                graph.get(key.charAt(0)).put(key.charAt(1), value);
            }
        });
    }


    public HashMap<String, Integer> loadFile(String fileName) {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());
        String absolutePath = file.getAbsolutePath();
        HashMap<String, Integer> edgesMap = new HashMap<>();
        try {
            File myObj = new File(absolutePath);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String[] edges = data.split(",");
                for (String edge : edges) {
                    edgesMap.put(edge.substring(0, 2), Integer.parseInt(edge.substring(2)));
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println(String.format("File %s not found, or couldn't be read", absolutePath));
            e.printStackTrace();
        }
        return edgesMap;
    }
}
