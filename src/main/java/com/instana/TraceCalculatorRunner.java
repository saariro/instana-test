package com.instana;

import java.util.*;

public class TraceCalculatorRunner {

    /**
     *
     * Application Runners Below
     *
     * */

    private static void runTask1(TraceCalculator calculator) {
        System.out.println(String.format("Task 1 Result: %d",calculator.calculateAverageTrace(Arrays.asList('A', 'B', 'C'))));

    }

    private static void runTask2(TraceCalculator calculator) {
        System.out.println(String.format("Task 2 Result: %d", calculator.calculateAverageTrace(Arrays.asList('A', 'D'))));
    }

    private static void runTask3(TraceCalculator calculator) {
        System.out.println(String.format("Task 3 Result: %d", calculator.calculateAverageTrace(Arrays.asList('A', 'D', 'C'))));

    }

    private static void runTask4(TraceCalculator calculator) {
        System.out.println(String.format("Task 4 Result: %d", calculator.calculateAverageTrace(Arrays.asList('A','E','B','C','D'))));

    }

    private static void runTask5(TraceCalculator calculator) {
        try {
            calculator.calculateAverageTrace(Arrays.asList('A','E','D'));
        } catch(TraceNotFoundException e) {
            System.out.println(String.format("Task 5 Result: %s", e.getMessage()));
        }

    }

    private static void runTask6(TraceCalculator calculator) {
        System.out.println(String.format("Task 6 Result: %d", calculator.calculateTracesWithMaxHops('C', 'C', 3)));
    }

    private static void runTask7(TraceCalculator calculator) {
        System.out.println(String.format("Task 7 Result: %d", calculator.calculateTracesWithExactHops('A', 'C', 4)));
    }

    private static void runTask8(TraceCalculator calculator) {
        System.out.println(String.format("Task 8 Result: %d", calculator.calculateShortedTrace('A', 'C')));

    }

    private static void runTask9(TraceCalculator calculator) {
        System.out.println(String.format("Task 9 Result: %d", calculator.calculateShortedTrace('B', 'B')));

    }

    private static void runTask10(TraceCalculator calculator) {
        System.out.println(String.format("Task 10 Result: %d", calculator.calculateAmountWithOfTracesWithMaxLatency('C', 'C', 30)));
    }

    private static void runTasks(TraceCalculator calculator) {
        runTask1(calculator);
        runTask2(calculator);
        runTask3(calculator);
        runTask4(calculator);
        runTask5(calculator);
        runTask6(calculator);
        runTask7(calculator);
        runTask8(calculator);
        runTask9(calculator);
        runTask10(calculator);

    }

    public static void main(String[] args) {
        String fileName = "testGraph.txt";
        if (args.length > 0 && (args[0] != "--help" || args[0] != "-h" )) {
            System.out.println("The usage:\n\nGive The file name to use for testing as an argument. If no argument is given the default file name testGraph.txt is used.\n\n\n\n");
            System.exit(0);
        }
        if (args.length > 0 && args[0] != null) {
            System.out.println(args[0]);
            fileName = args[0];
        }
        TraceCalculator calculator = new TraceCalculator();
        HashMap<String, Integer>  edges = calculator.loadFile(fileName);
        calculator.buildGraph(edges);
        runTasks(calculator);

    }
}
