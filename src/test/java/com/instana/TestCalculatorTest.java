package com.instana;

import org.junit.*;

import java.util.*;

import static org.junit.Assert.assertEquals;


public class TestCalculatorTest {

    public TraceCalculator calculator;
    private final String TEST_FILE = "testGraph.txt";

    private HashMap<String, Integer> edges;

    @Before
    public void setup() {
        calculator = new TraceCalculator();
        edges = calculator.loadFile(TEST_FILE);
        calculator.buildGraph(edges);
    }

    // Test input: 1
    @Test
    public void givenABCThenReturnCorrectAverageTrace() {
        int routeLength = calculator.calculateAverageTrace(Arrays.asList('A', 'B', 'C'));
        assertEquals(9, routeLength);

    }

    // Test input: 2
    @Test
    public void givenADThenReturnCorrectAverageTrace() {
        int routeLength = calculator.calculateAverageTrace(Arrays.asList('A', 'D'));
        assertEquals(5, routeLength);
    }

    // Test input: 3
    @Test
    public void givenADCThenReturnCorrectAverageTrace() {
        int routeLength = calculator.calculateAverageTrace(Arrays.asList('A', 'D', 'C'));
        assertEquals(13, routeLength);
    }

    // Test input: 4
    @Test
    public void givenAEBCDThenReturnCorrectAverageTrace() {
        int routeLength = calculator.calculateAverageTrace(Arrays.asList('A','E','B','C','D'));
        assertEquals(22, routeLength);
    }

    // Test input: 5
    @Test(expected = TraceNotFoundException.class)
    public void givenAEDThenReturnCorrectAverageTrace() {
        calculator.calculateAverageTrace(Arrays.asList('A','E','D'));
    }

    // Test input: 6
    @Test
    public void givenCCThenShouldFindMaxAmountOfHops() {
        int routeLength = calculator.calculateTracesWithMaxHops('C', 'C', 3);
        assertEquals(2, routeLength);
    }

    // Test input: 7
    @Test
    public void givenACThenShouldFindExactAmountOfHops() {
        int routeLength = calculator.calculateTracesWithExactHops('A', 'C', 4);
        assertEquals(3, routeLength);
    }

    // Test input: 8
    @Test
    public void givenACThenShouldFindCorrectShortestTrace() {
        int routeLength = calculator.calculateShortedTrace('A', 'C');
        assertEquals(9, routeLength);
    }

    // Test input: 9
    @Test
    public void givenBBThenShouldFindCorrectShortestTrace() {
        int routeLength = calculator.calculateShortedTrace('B', 'B');
        assertEquals(9, routeLength);
    }

    // Test input: 10
    @Test
    public void givenCCThenShouldCalculateDifferentTraces() {
        int routeLength = calculator.calculateAmountWithOfTracesWithMaxLatency('C', 'C', 30);
        assertEquals(7, routeLength);
    }
}
